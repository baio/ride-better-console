app.controller "resortsListCtl", ($scope, $sce, toolbar, spotsEP, locator, $mdBottomSheet) ->

  console.log "resortsListCtl.coffee:2 >>>"

  $scope.search = 
    searchStr : null
    results : []
    
  setList = (res) ->
    item.label = $sce.trustAsHtml(item.label) for item in res
    $scope.search.results = res
  
  loadList = (term, geo) ->
    spotsEP.get(term : term, geo : geo?.join(",")).then setList
  
  find = (term) ->
    locator.geo()
    .then (pos)-> loadList(term, pos)
    .catch -> loadList(term)

  $scope.$watch "search.searchStr", (val) ->
    find val