app.controller "resortContactsCtl", ($scope, toolbar, resortsEP, origin) ->

  console.log "resortInfoCtl.coffee:2 >>>"
  
  $scope.resort =         
    conatcts : []

  saveContacts = ->    
    resortsEP.putContacts(origin.resort._id, contacts : $scope.resort.contacts).then setContacts

  $scope.$on "toolbar::reset", ->
    loadContacts().then ->
      $scope.contactsForm.$setUntouched()
      $scope.contactsForm.$setPristine()

  $scope.$on "toolbar::save", ->
    if !$scope.contactsForm.$invalid
      saveContacts().then ->
        $scope.contactsForm.$setUntouched()
        $scope.contactsForm.$setPristine()            

  setContacts = (res) ->
    origin.resort = res
    $scope.resort =         
      contacts : if res.contacts then res.contacts else []

  loadContacts = ->      
    resortsEP.getInfo(origin.resort._id).then setContacts

  setContacts origin.resort

  updatePristine = ->
    if $scope.contactsForm.$pristine
      toolbar.setPristine()      
    else
      toolbar.setDirty()

  $scope.$watch "contactsForm.$pristine", (val) ->
    updatePristine()

  $scope.add = ->
    $scope.contactsForm.$setDirty()
    $scope.resort.contacts.splice 0, 0, type : "phone", val : null

  $scope.remove = (contact) ->
    $scope.contactsForm.$setDirty()
    $scope.resort.contacts.splice $scope.resort.contacts.indexOf(contact), 1
