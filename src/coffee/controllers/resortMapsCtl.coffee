app.controller "resortMapsCtl", ($scope, toolbar, resortsEP, origin) ->

  console.log "resortMapsCtl.coffee:2 >>>"
  
  $scope.resort = maps : []         

  setMap = (res) ->
    $scope.resort.maps[0].src = res.src

  setMaps = (resort) ->
    origin.resort = resort
    $scope.resort =         
      maps : if resort.maps then resort.maps else []    

  saveMap = ->    
    resortsEP.postMap(origin.resort._id, $scope.resort.maps[0])
    .then setMap

  $scope.$on "toolbar::reset", ->
    loadMaps().then ->
      $scope.mapsForm.$setUntouched()
      $scope.mapsForm.$setPristine()

  $scope.$on "toolbar::save", ->
    if !$scope.mapsForm.$invalid
      saveMap().then ->
        $scope.mapsForm.$setUntouched()
        $scope.mapsForm.$setPristine()    

  loadMaps = ->      
    resortsEP.getInfo(origin.resort._id).then setMaps

  setMaps origin.resort

  updatePristine = ->
    if $scope.mapsForm.$pristine
      toolbar.setPristine()      
    else
      toolbar.setDirty()

  $scope.$watch "mapsForm.$pristine", (val) ->
    updatePristine()

  $scope.addMap = (url) ->
    $scope.mapsForm.$setDirty()
    $scope.resort.maps.splice 0, 0, src : url

  $scope.removeMap = (val) ->
    ###
    $scope.mapsForm.$setDirty()
    $scope.resort.maps.splice $scope.resort.maps.indexOf(val), 1
    ###
    resortsEP.removeMap(origin.resort._id, val.src)
    .then ->
      $scope.resort.maps.splice $scope.resort.maps.indexOf(val), 1


