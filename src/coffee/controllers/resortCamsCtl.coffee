app.controller "resortCamsCtl", ($scope, toolbar, resortsEP, origin) ->

  console.log "resortCamsCtl.coffee:2 >>>"
  
  $scope.resort = cams : []         

  saveCams = ->    
    cams = $scope.resort.cams
    for cam in cams
      if cam.meta and cam.meta.type == "stream" and !cam.meta.src
        delete cam.meta
    resortsEP.putWebcams("1936", webcams : cams).then setCams

  $scope.$on "toolbar::reset", ->
    loadCams().then ->
      $scope.camsForm.$setUntouched()
      $scope.camsForm.$setPristine()

  $scope.$on "toolbar::save", ->
    if !$scope.camsForm.$invalid
      saveCams().then ->
        $scope.camsForm.$setUntouched()
        $scope.camsForm.$setPristine()    

  setCams = (res) ->
    origin.resort = res
    $scope.resort =         
      cams : res.webcams

  loadCams = ->      
    resortsEP.getInfo(origin.resort._id).then setCams

  setCams origin.resort

  updatePristine = ->
    if $scope.camsForm.$pristine
      toolbar.setPristine()      
    else
      toolbar.setDirty()

  $scope.$watch "camsForm.$pristine", (val) ->
    updatePristine()

  $scope.addCam = ->
    $scope.camsForm.$setDirty()
    $scope.resort.cams.splice 0, 0, meta : {src : null, type : "stream"}, index : null, title : null

  $scope.removeCam = (val) ->
    $scope.camsForm.$setDirty()
    $scope.resort.cams.splice $scope.resort.cams.indexOf(val), 1

