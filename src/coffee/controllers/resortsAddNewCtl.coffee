app.controller "resortsAddNewCtl", ($scope, $sce, toolbar, resortsEP, $mdBottomSheet, $state) ->

  console.log "resortsAddNewCtl.coffee:2 >>>"

  $scope.resort = null

  $scope.$on "toolbar::addNew", ->        
    $mdBottomSheet.show(
      templateUrl: "resort/resort-add-new.html"
      controller: "resortsAddNewCtl"
    )

  $scope.create = ->
    if $scope.addNewForm.$valid
      resortsEP.post($scope.resort).then ->
        window.location.hash  = "resort/#{$scope.resort.id}/info"
        $mdBottomSheet.hide()

