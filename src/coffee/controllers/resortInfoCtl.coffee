app.controller "resortInfoCtl", ($scope, resortsEP, toolbar, origin, locator, $timeout) ->
  
  console.log "resortInfoCtl.coffee:2 >>>"
  
  $scope.resort = {}

  
  fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI?.html5 != false)
  console.log "resortInfoCtl.coffee:9 >>>", fileReaderSupported
  
  $scope.$on "toolbar::reset", ->
    loadInfo().then ->
      $scope.infoForm.$setUntouched()
      $scope.infoForm.$setPristine()

  $scope.$on "toolbar::save", ->
    if !$scope.infoForm.$invalid
      saveInfoMain().then (res) ->
        console.log "resortInfoCtl.coffee:19 >>>", res
        $scope.infoForm.$setUntouched()
        $scope.infoForm.$setPristine()        
  
  setInfo = (res) ->
    origin.resort = res
    geo = res.geo
    $scope.resort =         
      headerFile : null
      title : res.title
      description : res.description
      header : res.header
      map : { center: { latitude: geo[0], longitude: geo[1] }, zoom: 12}
      mapOptions : { mapTypeId: google.maps.MapTypeId.SATELLITE } 
      mapMarker : if geo then { latitude: geo[0], longitude: geo[1] }

  saveInfoMain = ->    

    data =
      title : $scope.resort.title
      description : $scope.resort.description  
      header : $scope.resort.header
      geo :  [$scope.resort.mapMarker.latitude, $scope.resort.mapMarker.longitude]
    
    resortsEP.putInfoMain(origin.resort._id, data, $scope.resort.headerFile?[0])
    .then setInfo

  loadInfo = ->      
    resortsEP.getInfo(origin.resort._id).then setInfo

  if !origin.resort.geo
    locator.geo().then (res) ->
      origin.resort.geo = res
      setInfo origin.resort
  else
    setInfo origin.resort

  updatePristine = ->
    if $scope.infoForm.$pristine
      toolbar.setPristine()      
    else
      toolbar.setDirty()

  $scope.$watch "infoForm.$pristine", (val) ->
    updatePristine()

  $scope.$watch "infoForm.$pristine", (val) ->
    updatePristine()

  $scope.$watch "markerSetter", (val) ->
    console.log "resortInfoCtl.coffee:72 >>>", val
    if val
      geo = val.split(",").map (m) -> m.trim()      
      $scope.resort.map.center = 
        latitude: geo[0]
        longitude: geo[1]
      $timeout ->
        $scope.resort.mapMarker = 
          latitude: geo[0]
          longitude: geo[1]
      

  $scope.markerDragEnd = ->    
    $scope.infoForm.$setDirty()

  $scope.setHeaderImageFromFile = (file) ->
    if file?
      if fileReaderSupported and file.type.indexOf("image") > -1
        fileReader = new FileReader()
        fileReader.readAsDataURL file
        fileReader.onload = (e) ->
          $scope.$evalAsync ->
            $scope.resort.header = e.target.result





