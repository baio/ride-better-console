app.controller "resortProsConsCtl", ($scope, toolbar, resortsEP, origin) ->

  console.log "resortProsCtl.coffee:2 >>>"
  
  $scope.resort =         
      pros : []
      cons : []


  saveProsCons = ->    
    console.log "resortProsConsCtl.coffee:11 >>>", $scope.resort
    data =
      pros : $scope.resort.pros.map (m) -> m.val
      cons : $scope.resort.cons.map (m) -> m.val
    resortsEP.putProsCons(origin.resort._id, proscons : data).then setProsCons

  $scope.$on "toolbar::reset", ->
    loadProsCons().then ->
      $scope.prosConsForm.$setUntouched()
      $scope.prosConsForm.$setPristine()

  $scope.$on "toolbar::save", ->
    if !$scope.prosConsForm.$invalid
      saveProsCons().then ->
        $scope.prosConsForm.$setUntouched()
        $scope.prosConsForm.$setPristine()    

  setProsCons = (res) ->
    origin.resort = res

    $scope.resort =         
      if res.proscons
        pros : res.proscons.pros.map (m) -> val : m
        cons : res.proscons.cons.map (m) -> val : m
      else
        pros : []
        cons : []

  loadProsCons = ->      
    resortsEP.getInfo(origin.resort._id).then setProsCons

  setProsCons origin.resort

  updatePristine = ->
    if $scope.prosConsForm.$pristine
      toolbar.setPristine()      
    else
      toolbar.setDirty()

  $scope.$watch "prosConsForm.$pristine", (val) ->
    updatePristine()

  $scope.addPros = ->
    $scope.prosConsForm.$setDirty()
    $scope.resort.pros.splice 0, 0, val : null

  $scope.addCons = ->
    $scope.prosConsForm.$setDirty()
    $scope.resort.cons.splice 0, 0, val : null

  $scope.removePros = (val) ->
    $scope.prosConsForm.$setDirty()
    $scope.resort.pros.splice $scope.resort.pros.indexOf(val), 1

  $scope.removeCons = (val) ->
    $scope.prosConsForm.$setDirty()
    $scope.resort.pros.splice $scope.resort.cons.indexOf(val), 1
