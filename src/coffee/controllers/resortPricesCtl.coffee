app.controller "resortPricesCtl", ($scope, toolbar, resortsEP, $q, origin) ->

  console.log "resortPricesCtl.coffee:2 >>>"
  
  $scope.resort = prices : []         

  getImageFromFile = (file) ->
    deferred = $q.defer()
    if file?
      if file.type.indexOf("image") > -1
        fileReader = new FileReader()
        fileReader.readAsDataURL file
        fileReader.onload = (e) ->
          deferred.resolve(file : file, src : e.target.result)
        fileReader.onerror = (e) ->
          deferred.reject(eresult)          
      else
        deferred.reject(new Error "Not a file")          
    deferred.promise

  uploadPrices = (prices) ->
    $q.all(prices.map (m) -> resortsEP.postPrice(origin.resort._id, m.file, title : m.title, tag : m.tag, href : m.href))
    .then (res) -> res[res.length - 1]

  savePrices = ->    
    upd = $scope.resort.prices.filter((f) -> f.file)
    prices = $scope.resort.prices.filter((f) -> !f.file).map (m) ->
      delete m.created
      m
    resortsEP.putPrices(origin.resort._id, prices : prices)
    .then (res) ->
      if upd.length
        uploadPrices(upd)
      else
        res
    .then setPrices

  $scope.$on "toolbar::reset", ->
    loadPrices().then ->
      $scope.pricesForm.$setUntouched()
      $scope.pricesForm.$setPristine()

  $scope.$on "toolbar::save", ->
    if !$scope.pricesForm.$invalid
      savePrices().then ->
        $scope.pricesForm.$setUntouched()
        $scope.pricesForm.$setPristine()    

  setPrices = (res) ->
    origin.resort = res
    $scope.priceFiles = null
    $scope.resort =         
      prices : if res.prices then res.prices else []

  loadPrices = ->      
    resortsEP.getInfo(origin.resort._id).then setPrices

  setPrices origin.resort

  updatePristine = ->
    if $scope.pricesForm.$pristine
      toolbar.setPristine()      
    else
      toolbar.setDirty()

  $scope.$watch "pricesForm.$pristine", (val) ->
    updatePristine()

  $scope.removePrice = (price) ->
    $scope.pricesForm.$setDirty()
    $scope.resort.prices.splice $scope.resort.prices.indexOf(price), 1

  $scope.setPricesFromFiles = (priceFiles) ->
    console.log "resortPricesCtl.coffee:48 >>>", priceFiles
    $scope.pricesForm.$setDirty()
    $q.all(priceFiles.map getImageFromFile).then (res) ->
      items = res.map (m) -> src : m.src, title : null, file : m.file
      $scope.$evalAsync ->
        $scope.resort.prices.splice 0, 0, items...
      

