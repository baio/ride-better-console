app.factory "locator", ($q, $rootScope) ->

  geo: ->
    deferred = $q.defer()
    navigator.geolocation.getCurrentPosition (pos)-> 
      $rootScope.$evalAsync ->
        deferred.resolve [pos.coords.latitude, pos.coords.longitude]
    , (err) ->
      $rootScope.$evalAsync ->
        deferred.reject [pos.coords.latitude, pos.coords.longitude]
    deferred.promise
