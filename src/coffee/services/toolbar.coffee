app.factory "toolbar", ($rootScope) ->

  _isSaveEnabled = false
  _isResetEnabled = false
  _isListEnabled = true
  _isAddEnabled = true

  isSaveEnabled: -> _isSaveEnabled

  isResetEnabled: -> _isResetEnabled

  isListEnabled: -> _isListEnabled

  isAddEnabled: -> _isAddEnabled

  setDirty: ->
    _isSaveEnabled = true
    _isResetEnabled = true
    _isListEnabled = false
    _isAddEnabled = false    

  setPristine: ->
    _isSaveEnabled = false
    _isResetEnabled = false
    _isListEnabled = true
    _isAddEnabled = true

  setLocked: ->
    _isSaveEnabled = false
    _isResetEnabled = true
    _isListEnabled = false
    _isAddEnabled = false

  reset: ->
    $rootScope.$broadcast("toolbar::reset")    

  save: ->
    $rootScope.$broadcast("toolbar::save")    

  addNew: ->
    $rootScope.$broadcast("toolbar::addNew")    

