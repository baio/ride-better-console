app.factory "resortsEP", (_ep) ->

  putWebcams: (spot, data) ->
    
      _ep.put "resorts/" + spot + "/webcams", data

  putPrices: (spot, data) ->
    
      _ep.put "resorts/" + spot + "/prices", data

  postPrice: (spot, file, data) ->
    
      _ep.postFile "resorts/" + spot + "/price", file, data

  getContacts: (spot) ->
    
      _ep.get "resorts/" + spot + "/contacts"

  putContacts: (spot, data) ->
    
      _ep.put "resorts/" + spot + "/contacts", data

  putProsCons: (spot, data) ->
    
      _ep.put "resorts/" + spot + "/proscons", data      

  postMap: (spot, data) ->
    
      _ep.post "resorts/" + spot + "/map", data      

  removeMap: (spot, url) ->
    
      _ep.remove "resorts/" + spot + "/maps/#{encodeURIComponent url}"

  getInfo: (spot) ->
    
      _ep.get "resorts/" + spot + "/info"

  getMaps: (spot) ->
        
      _ep.get "resorts/" + spot + "/maps"


  getPrices: (spot) ->
        
      _ep.get "resorts/" + spot + "/prices"

  putInfoMain: (spot, data, file) ->
    
    ###
    if !file
      _ep.put "resorts/" + spot + "/info", data
    else
    ###
    
    _ep.putFile "resorts/" + spot + "/info", file, data    

  postInfoHeader: (spot, file) ->
  
    _ep.postFile "resorts/" + spot + "/header", file

  post: (data) ->
  
    _ep.post "resorts", data
