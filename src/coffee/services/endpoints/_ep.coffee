app.factory "_ep", ($q, $http, webApiConfig, notifier, $upload) ->

  getAuthHeaders = -> authorization: "Bearer " + authio.getJWT()

  saveFile = (method, path, file, data, useAuth) ->

    inProgress = false

    if inProgress
      deferred.reject(new Error "In progress")
    else
      notifier.showLoading()
      inProgress = true

    headers = getAuthHeaders() if useAuth

    $upload.upload
      url: webApiConfig.url + path
      method : method
      file: file
      data : data
    .then (res) ->
      res.data
    .finally ->
      inProgress = false
      notifier.hideLoading()

  save = (method, path, data, useAuth) ->
  
    deferred = $q.defer()

    inProgress = false

    if inProgress
      deferred.reject(new Error "In progress")
    else
      notifier.showLoading()
      inProgress = true
      headers = headers : getAuthHeaders() if useAuth
      $http[method](webApiConfig.url + path, data, headers).success (data) ->
        inProgress = false
        deferred.resolve data
      .error (data) ->
        deferred.reject data
      .finally ->
        inProgress = false
        notifier.hideLoading()

    deferred.promise


  get : (path, qs) ->

    deferred = $q.defer()

    inProgress = false

    if inProgress
      deferred.reject(new Error "In progress")
    else
      notifier.showLoading()
      inProgress = true
      $http.get(webApiConfig.url + path, params : qs)
        .success(deferred.resolve)
        .error(deferred.reject)
        .finally ->
          inProgress = false
          notifier.hideLoading()

    deferred.promise


  post : (path, data, useAuth) ->
    save "post", path, data, useAuth

  put : (path, data, useAuth) ->
    save "put", path, data, useAuth

  remove : (path, useAuth) ->

    deferred = $q.defer()

    inProgress = false

    if inProgress
      deferred.reject(new Error "In progress")
    else
      notifier.showLoading()
      inProgress = true
      headers = headers : getAuthHeaders() if useAuth
      $http.delete(webApiConfig.url + path, headers).success (data) ->
        inProgress = false
        deferred.resolve data
      .error (data) ->
          deferred.reject data
      .finally ->
          inProgress = false
          notifier.hideLoading()

    deferred.promise

  postFile: (path, file, data, useAuth) ->
    saveFile "post", path, file, data, useAuth

  putFile: (path, file, data, useAuth) ->
    saveFile "put", path, file, data, useAuth
