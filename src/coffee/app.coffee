app = angular.module("ride-better-console", ["ngMaterial", "ui.router", "uiGmapgoogle-maps", "ngMessages", "angularFileUpload"])

app.config ($stateProvider, $urlRouterProvider) ->    

    $urlRouterProvider.otherwise("/resorts/list")

    $stateProvider
    .state("resort", 
        url: "/resort/:id"
        resolve: 
            origin: ($stateParams, resortsEP) ->                 
                resortsEP.getInfo($stateParams.id).then (res) ->
                    resort : res        
        views: 
            content:
                templateUrl: "resort/resort.html"
            nav:
                templateUrl: "resort/resort-nav.html"
                controller: ($scope, origin) ->
                    $scope.origin = origin                
            toolbar:
                templateUrl: "resort/resort-toolbar.html"                
            "nav-toolbar":
                templateUrl: "resort/resort-nav-toolbar.html"                                
                controller: ($scope, origin) ->
                    $scope.origin = origin

    )
    .state("resort.info", 
        url: "/info"
        views:
            "resort-content" :
                templateUrl: "resort/resort-info.html"
                controller : "resortInfoCtl"
    )
    .state("resort.contacts", 
        url: "/contacts",
        views:
            "resort-content" :    
                templateUrl: "resort/resort-contacts.html"
                controller : "resortContactsCtl"
    )
    .state("resort.proscons", 
        url: "/proscons",
        views:
            "resort-content" :        
                templateUrl: "resort/resort-proscons.html"
                controller : "resortProsConsCtl"
    )
    .state("resort.maps", 
        url: "/maps"
        views:
            "resort-content" :        
                templateUrl: "resort/resort-maps.html"
                controller : "resortMapsCtl"
    )
    .state("resort.prices", 
        url: "/prices"
        views:
            "resort-content" :        
                templateUrl: "resort/resort-prices.html"
                controller : "resortPricesCtl"
    )
    .state("resort.cams", 
        url: "/cams"
        views:
            "resort-content" :        
                templateUrl: "resort/resort-cams.html"
                controller : "resortCamsCtl"
    )
    .state("resorts-list", 
        url: "/resorts/list"
        views: 
            content:
                templateUrl: "resorts-list/resorts-list.html"
                controller : "resortsListCtl"                
            toolbar:
                templateUrl: "resorts-list/resorts-list-toolbar.html"                
                controller : "resortsAddNewCtl"
    )
    