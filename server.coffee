express = require('express')

$PORT = Number(process.env.PORT)

app = express()
app.use(express.static(__dirname + "/www"))
.listen($PORT)

console.log "server run on port #{$PORT}"