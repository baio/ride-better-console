"use strict"

gulp = require("gulp")
nodemon = require "nodemon"
plumber = require "gulp-plumber"
coffee = require "gulp-coffee"
jade = require "gulp-jade"
concat = require "gulp-concat"
lr = require "tiny-lr"

lrServer = lr()

gulp.task "jade", ->

  gulp.src("./src/jade/index.jade")
  .pipe(plumber())
  .pipe(jade(pretty : true))
  .pipe(concat("index.html"))
  .pipe(gulp.dest("./www"))


buildCoffee = ->
  gulp.src([ "./src/coffee/**/*.coffee"])
  .pipe(plumber())
  .pipe(coffee(bare: true))
  .pipe(concat("app.js"))
  .pipe(gulp.dest("./www"))

gulp.task "coffee",  buildCoffee

gulp.task "watch-jade", ["jade"], ->
  gulp.watch "./src/jade/**/*.jade", ["jade"]

gulp.task "watch-coffee", ["coffee"], ->
  gulp.watch "./src/coffee/**/*.coffee", ["coffee"]

gulp.task "watch-assets", ->
  lrServer.listen 35729, (err) ->
    if err
      console.log err
  gulp.watch ["./www/app.js", "./www/css/*.css", "./www/index.html"], (file) ->
    lrServer.changed body : files: [file.path]

gulp.task "nodemon", ->
  nodemon(script : "./server.coffee")

gulp.task "build", ["coffee", "jade"], ->


gulp.task "default", ["watch-jade", "watch-coffee", "watch-assets", "nodemon"]

